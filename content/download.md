---
title: Download
layout: download
appstream: org.kde.elisa
name: Elisa
gear: true
menu:
  main:
    weight: 2
sources:
  - name: Flatpak
    src_icon: /reusable-assets/flatpak.png
    description: |
      **Add flathub repository using:**

      ```bash
      flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
      ```

      **Install Elisa**

      ```bash
      flatpak install flathub org.kde.elisa
      ```
  - name: Windows
    src_icon: /reusable-assets/windows.svg
    description: |
      * [Microsoft store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)
      * [Win64 executable](https://binary-factory.kde.org/view/Windows%2064-bit/job/Elisa_Release_win64/)
---
