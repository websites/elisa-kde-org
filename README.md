# Elisa Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_elisa-kde-org)](https://binary-factory.kde.org/job/Website_elisa-kde-org/)

This is the git repository for [elisa.kde.org](https://elisa.kde.org), the website for Elisa.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
