#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: websites-elisa-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-05 00:52+0000\n"
"PO-Revision-Date: 2022-03-04 09:18-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: config.yaml:0 content/_index.md:0 content/download.md:0
#: content/get-involved.md:0 content/users.md:0 i18n/en.yaml:0
msgid "Elisa"
msgstr "Elisa"

#: config.yaml:0
msgid "A music player that is simple, reliable, and a joy to use."
msgstr "Um reprodutor de música simples, confiável e divertido de usar."

#: config.yaml:0
msgid "Donate"
msgstr "Doar"

#: content/download.md:0 i18n/en.yaml:0
msgid "Windows"
msgstr "Windows"

#: content/download.md:0
msgid "Download"
msgstr "Baixar"

#: content/download.md:0
msgid "Flatpak"
msgstr "Flatpak"

#: content/download.md:0
msgid ""
"**Add flathub repository using:**\n"
"\n"
"```bash\n"
"flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub."
"flatpakrepo\n"
"```\n"
"\n"
"**Install Elisa**\n"
"\n"
"```bash\n"
"flatpak install flathub org.kde.elisa\n"
"```\n"
msgstr ""
"**Adicione o repositório flathub usando:**\n"
"\n"
"```bash\n"
"flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub."
"flatpakrepo\n"
"```\n"
"\n"
"**Instale o Elisa**\n"
"\n"
"```bash\n"
"flatpak install flathub org.kde.elisa\n"
"```\n"

#: content/download.md:0
msgid ""
"* [Microsoft store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)\n"
"* [Win64 executable](https://binary-factory.kde.org/view/Windows%2064-bit/"
"job/Elisa_Release_win64/)"
msgstr ""
"* [Microsoft Store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)\n"
"* [Executável Win64](https://binary-factory.kde.org/view/Windows%2064-bit/"
"job/Elisa_Release_win64/)"

#: content/get-involved.md:0
msgid "Get Involved"
msgstr "Participe"

#: content/get-involved.md:0
msgid ""
"Most development-related discussions take place on the [Elisa mailing list]"
"(http://mail.kde.org/mailman/listinfo/elisa).\n"
"Just join in, say hi and tell us what you would like to help us with!"
msgstr ""
"A maioria das discussões relativas ao desenvolvimento tem lugar na [lista de "
"discussão do Elisa] (http://mail.kde.org/mailman/listinfo/elisa).\n"
"Junte-se, diga oi e conte-nos como você gostaria de nos ajudar!"

#: content/get-involved.md:14
msgid ""
"Want to contribute to Elisa? Check out [Phabricator](https://phabricator.kde."
"org/tag/elisa/) for some fun task or [browse the source code](https://invent."
"kde.org/multimedia/elisa)."
msgstr ""
"Quer contribuir com o Elisa? Veja o [Phabricator](https://phabricator.kde."
"org/tag/elisa/) para algumas tarefas divertidas ou [navegue pelo código-"
"fonte](https://invent.kde.org/multimedia/elisa)."

#: content/users.md:0
msgid "User Support"
msgstr "Suporte ao usuário"

#: content/users.md:0
msgid "https://docs.kde.org/stable5/en/elisa/elisa/index.html"
msgstr "https://docs.kde.org/stable5/pt_BR/elisa/elisa/index.html"

#: i18n/en.yaml:0
msgid "Linux"
msgstr "Linux"

#: i18n/en.yaml:0
msgid "FreeBSD"
msgstr "FreeBSD"

#: i18n/en.yaml:0
msgid "Other ways to download"
msgstr "Outras formas de baixar"

#: i18n/en.yaml:0
msgid ""
"<b>Elisa</b> is a music player developed by the KDE community that strives "
"to be simple and nice to use. We also recognize that we need a flexible "
"product to account for the different workflows and use-cases of our users. "
"We focus on a very good integration with the Plasma desktop of the KDE "
"community without compromising support for other platforms (other Linux "
"desktop environments, Windows, and Android). We are creating a reliable "
"product that is a joy to use and respects our users' privacy. As such, we "
"prefer to support online services where users are in control of their data."
msgstr ""
"<b>Elisa</b> é um reprodutor de música desenvolvido pela comunidade KDE que "
"se esforça para ser simples e agradável de usar. Também reconhecemos que "
"precisamos de um produto flexível para atender aos diferentes fluxos de "
"trabalho e casos de uso de nossos usuários. Nós nos concentramos em uma "
"integração muito boa com a área de trabalho Plasma da comunidade KDE sem "
"comprometer o suporte para outras plataformas (outros ambientes de trabalho "
"Linux, Windows e Android). Estamos criando um produto confiável que é uma "
"divertido de usar e que respeita a privacidade de nossos usuários. Como tal, "
"preferimos oferecer suporte a serviços online em que os usuários controlem "
"seus dados."

#: i18n/en.yaml:0
msgid "Available on all major platforms"
msgstr "Disponível em todas as principais plataformas"

#: i18n/en.yaml:0
msgid "Browse music by album, artist, or tracks"
msgstr "Navegue pelas músicas por álbum, artista ou faixas"

#: i18n/en.yaml:0
msgid ""
"Elisa, powered with Baloo indexing support, automatically looks in your "
"music folder for music files and cover art. The left column has various "
"viewing options: Now Playing, Albums, Artists, and Tracks."
msgstr ""
"Elisa, com suporte para indexação Baloo, procura automaticamente em sua "
"pasta de música por arquivos de música e arte de capa. A coluna da esquerda "
"possui várias opções de visualização: reproduzindo agora, álbuns, artistas e "
"faixas."

#: i18n/en.yaml:0
msgid "Create and manage playlists"
msgstr "Criar e gerenciar lista de músicas"

#: i18n/en.yaml:0
msgid "Create and manage all your playlists from the built-in side panel."
msgstr ""
"Criar e gerenciar todas as suas listas de músicas no painel lateral "
"integrado."

#: i18n/en.yaml:0
msgid "Screenshot of the playlist feature"
msgstr "Captura de tela do recurso da lista de músicas"

#: i18n/en.yaml:0
msgid "Party mode"
msgstr "Modo festa"

#: i18n/en.yaml:0
msgid "Turn Party Mode on with Elisa's immersive built-in party mode feature."
msgstr ""
"Ligue o modo festa com o recurso de modo festa imersivo integrado do Elisa."

#: i18n/en.yaml:0
msgid "Screenshot of Elisa's party mode"
msgstr "Captura de tela do modo festa do Elisa"

#: i18n/en.yaml:0
msgid "Dark / Light Themes"
msgstr "Temas claros/escuros"

#: i18n/en.yaml:0
msgid ""
"Elisa has both a <b>Light</b> and a <b>Dark</b> theme, so you can enjoy your "
"music without the colors bothering you."
msgstr ""
"O Elisa tem ambos os temas <b>claro</b> e <b>escuro</b>, para que você possa "
"curtir suas músicas sem que as cores o incomodem."

#: i18n/en.yaml:0
msgid "Fast indexing"
msgstr "Indexação rápida"

#: i18n/en.yaml:0
msgid ""
"With the help of <a href=\"https://community.kde.org/Baloo\">Baloo</a>, "
"Elisa is fast to index and sort all your files. It is also possible to use "
"Elisa without Baloo."
msgstr ""
"Com a ajuda do <a href=\"https://community.kde.org/Baloo\">Baloo</a>, o "
"Elisa é rápido para indexar e ordenar todo os seus arquivos. Também é "
"possível usar o Elisa sem o Baloo."

#: i18n/en.yaml:0
msgid "Adapts to your screen"
msgstr "Adapta-se a sua tela"

#: i18n/en.yaml:0
msgid ""
"Elisa uses <a href=\"https://develop.kde.org/frameworks/kirigami/"
"\">Kirigami</a> to be the best convergent, responsive music player that "
"adapts its content to your screen. Soon on your phone!"
msgstr ""
"Elisa usa o <a href=\"https://develop.kde.org/frameworks/kirigami/"
"\">Kirigami</a> para ser o melhor reprodutor de música convergente e "
"responsivo que adapta seu conteúdo à sua tela. Em breve no seu celular!"

#: i18n/en.yaml:0
msgid "Open Source"
msgstr "Código aberto"

#: i18n/en.yaml:0
msgid ""
"Elisa is Open Source and you can browse, edit and share the source code.  "
"Elisa is <a href=\"https://kde.org\">Made By KDE</a>, a community building "
"high-quality projects that your are free to use. Check out all <a href="
"\"https://kde.org/products\"> our projects!</a>"
msgstr ""
"Elisa é código aberto e você pode navegar, editar e compartilhar o código-"
"fonte. Elisa é <a href=\"https://kde.org\">feito pelo KDE</a>, uma "
"comunidade que cria projetos de alta qualidade que você pode usar "
"gratuitamente. Confira todos os <a href=\"https://kde.org/products\">nossos "
"projetos!</a>"

#~ msgid "org.kde.elisa.desktop"
#~ msgstr "org.kde.elisa.desktop"

#~ msgid "/assets/img/windows.png"
#~ msgstr "/assets/img/windows.png"

#~ msgid "https://forum.kde.org/"
#~ msgstr "https://forum.kde.org/"
